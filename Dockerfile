FROM php:apache

WORKDIR /var/www/html
ADD index.php /var/www/html

RUN cp -f "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN sed -i "s/^expose_php.*/expose_php\ =\ Off/g" "$PHP_INI_DIR/php.ini"
RUN sed -i "s/^ServerTokens.*/ServerTokens\ Prod/g" "$APACHE_CONFDIR/conf-available/security.conf"
