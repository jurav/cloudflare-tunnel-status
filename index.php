<?php

$cf_account_id=getenv('cf_account_identifier', true) ?: getenv('CF_ACCOUNT_ID');
$cf_tunnel_id=getenv('cf_tunnel_id', true) ?: getenv('CF_TUNNEL_ID');
$cf_token=getenv('cf_token', true) ?: getenv('CF_TOKEN');

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.cloudflare.com/client/v4/accounts/$cf_account_id/cfd_tunnel/$cf_tunnel_id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 10,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer $cf_token",
          "Content-Type: application/json",
  ),
));

$response = curl_exec($curl);
$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

$obj = json_decode($response, true);
http_response_code($httpcode);

if ($httpcode != 200) {
    $api_error_code = $obj['errors'][0]['code'];
    $api_error_message = $obj['errors'][0]['message'];

    echo $api_error_code.': '.$api_error_message;
    exit;
} else {
    $tunnel_name = $obj['result']['name'];
    $tunnel_status = $obj['result']['status'];

    if ($tunnel_status == 'down') {
        http_response_code(503);
    }
    echo $tunnel_name.': '.$tunnel_status;
}

?>
