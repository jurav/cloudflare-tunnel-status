# cloudflare-tunnel-status


## Build image

```
docker build . -t cloudflare-tunnel-status:latest
docker tag cloudflare-tunnel-status:latest jurav/cloudflare-tunnel-status:latest
docker push jurav/cloudflare-tunnel-status:latest
```

## Docker hub

- https://hub.docker.com/r/jurav/cloudflare-tunnel-status

## Docker compose usage
```
version: '2'

services:
  tunnel-check:
    image: jurav/cloudflare-tunnel-status:latest
    restart: unless-stopped
    environment:
      - CF_TOKEN=xxxx
      - CF_ACCOUNT_ID=xxxx
      - CF_TUNNEL_ID=xxxx
    ports:
      - 8888:80
```
